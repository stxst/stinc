<?php
/**
 * Custom Search
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2023-11-06
 */

namespace st\search;

require_once __DIR__ . '/ref/custom-search.php';

/**
 * Adds post meta keys that are handled as searching target.
 *
 * @param string|string[] $key_s Post meta keys.
 */
function add_meta_key( $key_s ): void {
	\wpinc\ref\add_meta_key( $key_s );
}

/**
 * Adds post type specific pages.
 *
 * @param string          $slug        Search page slug.
 * @param string|string[] $post_type_s Post types.
 */
function add_post_type_specific_page( string $slug, $post_type_s ): void {
	\wpinc\ref\add_post_type_specific_page( $slug, $post_type_s );
}

/** phpcs:ignore
 * Activates the search.
 *
 * phpcs:ignore
 * @param array{
 *     home_url_function?    : callable,
 *     target_post_types?    : string[],
 *     do_allow_slash?       : bool,
 *     do_target_post_meta?  : bool,
 *     do_enable_blank_query?: bool,
 *     do_enable_custom_page?: bool,
 *     do_extend_query?      : bool,
 *     blank_query_title?    : string,
 * } $args Arguments.
 *
 * $args {
 *     Arguments.
 *
 *     @type 'home_url_function'     Callable for getting home URL link. Default '\home_url'.
 *     @type 'target_post_types'     Target post types. Default empty (no limitation).
 *     @type 'do_allow_slash'        Whether do allow slash in query. Default true.
 *     @type 'do_target_post_meta'   Whether do search for post meta values. Default true.
 *     @type 'do_enable_blank_query' Whether do enable blank query. Default true.
 *     @type 'do_enable_custom_page' Whether do enable custom search page. Default false.
 *     @type 'do_extend_query'       Whether do extend query. Default false.
 *     @type 'blank_query_title'     Document title when query is empty. Default ''.
 * }
 */
function activate( array $args = array() ): void {
	\wpinc\ref\activate( $args );
}
