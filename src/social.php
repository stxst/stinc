<?php
/**
 * Social Media
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2023-11-06
 */

namespace st;

require_once __DIR__ . '/socio/assets/url.php';
require_once __DIR__ . '/socio/analytics.php';
require_once __DIR__ . '/socio/open-graph-protocol.php';
require_once __DIR__ . '/socio/share-link.php';
require_once __DIR__ . '/socio/site-meta.php';
require_once __DIR__ . '/socio/structured-data.php';

/** phpcs:ignore
 * Outputs google analytics code.
 *
 * phpcs:ignore
 * @param array{
 *     url_to?           : string,
 *     site_verification?: string,
 *     tag_id?           : string,
 *     do_show_dialog?   : bool,
 *     expired_day?      : int,
 *     id_dialog?        : string,
 *     id_accept?        : string,
 *     id_reject?        : string,
 * } $args Arguments.
 *
 * $args {
 *     Arguments.
 *
 *     @type string 'url_to'            URL to this script.
 *     @type string 'site_verification' The site verification code.
 *     @type string 'tag_id'            The google tag ID.
 *     @type bool   'do_show_dialog'    Whether to show the dialog.
 *     @type int    'expired_day'       The length of keeping settings.
 *     @type string 'id_dialog'         Element ID of the dialog. Defaults 'wpinc-socio-analytics-dialog'.
 *     @type string 'id_accept'         Element ID of the accept button. Defaults 'wpinc-socio-analytics-accept'.
 *     @type string 'id_reject'         Element ID of the reject button. Defaults 'wpinc-socio-analytics-reject'.
 * }
 */
function the_google_analytics_code( array $args = array() ): void {
	\wpinc\socio\the_google_analytics_code( $args );
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Outputs the open graph protocol meta tags.
 *
 * phpcs:ignore
 * @param array{
 *     default_image_url?  : string,
 *     do_append_site_name?: bool,
 *     separator?          : string,
 *     excerpt_length?     : int,
 *     alt_description?    : string,
 *     image_size?         : string,
 *     image_meta_key?     : string,
 *     alt_image_url?      : string,
 * } $args (Optional) Options.
 *
 * $args {
 *     (Optional) Options.
 *
 *     @type string 'default_image_url'   Default image URL.
 *     @type bool   'do_append_site_name' Whether the site name is appended.
 *     @type string 'separator'           Separator between the page title and the site name.
 *     @type int    'excerpt_length'      The length of excerpt.
 *     @type string 'alt_description'     Alternative description.
 *     @type string 'image_size'          The image size.
 *     @type string 'image_meta_key'      Meta key of image.
 *     @type string 'alt_image_url'       Alternative image URL.
 * }
 */
function the_ogp( array $args = array() ): void {
	\wpinc\socio\the_ogp( $args );
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Outputs share links.
 *
 * phpcs:ignore
 * @param array{
 *     before?             : string,
 *     after?              : string,
 *     before_link?        : string,
 *     after_link?         : string,
 *     do_append_site_name?: bool,
 *     separator?          : string,
 *     media?              : string[],
 * } $args (Optional) Post navigation arguments.
 *
 * $args {
 *     (Optional) Post navigation arguments.
 *
 *     @type string   'before'              Markup to prepend to the all links.
 *     @type string   'after'               Markup to append to the all links.
 *     @type string   'before_link'         Markup to prepend to each link.
 *     @type string   'after_link'          Markup to append to each link.
 *     @type bool     'do_append_site_name' Whether the site name is appended.
 *     @type string   'separator'           Separator between the page title and the site name.
 *     @type string[] 'media'               Social media names.
 * }
 */
function the_share_links( array $args = array() ): void {
	\wpinc\socio\the_share_links( $args );
}


// -----------------------------------------------------------------------------


/**
 * Sets site icon (favicon).
 *
 * @param string                  $dir_url The url to image directory.
 * @param array<int, string>|null $icons   Array of icon sizes to icon file names.
 */
function set_site_icon( string $dir_url, ?array $icons = null ): void {
	\wpinc\socio\set_site_icon( $dir_url, $icons );
}

/**
 * Outputs the site description.
 */
function the_site_description(): void {
	\wpinc\socio\the_site_description();
}

/**
 * Retrieves the title of the current page.
 *
 * @param bool   $do_append_site_name Whether the site name is appended.
 * @param string $separator           Separator between the page title and the site name.
 * @return string The title.
 */
function get_the_title( bool $do_append_site_name, string $separator ): string {
	return \wpinc\socio\get_the_title( $do_append_site_name, $separator );
}

/**
 *
 * Retrieves the website name.
 *
 * @return string The name of the website.
 */
function get_site_name(): string {
	return \wpinc\socio\get_site_name();
}

/**
 *
 * Retrieves the website description.
 *
 * @return string The description of the website.
 */
function get_site_description(): string {
	return \wpinc\socio\get_site_description();
}

if ( ! function_exists( '\st\get_current_url' ) ) {
	/**
	 * Gets current URL.
	 *
	 * @return string|false Current URL.
	 */
	function get_current_url() {
		return \wpinc\get_current_url();
	}
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Outputs the structured data.
 *
 * phpcs:ignore
 * @param array{
 *     url?        : string,
 *     name?       : string,
 *     in_language?: string,
 *     description?: string,
 *     same_as?    : string[],
 *     logo?       : string,
 *     publisher?  : array{
 *         '@type'?: string,
 *         name? : string,
 *         logo? : string,
 *     }
 * } $args (Optional) The data of the website.
 *
 * $args {
 *     (Optional) The data of the website.
 *
 *     @type string                'url'         The URL.
 *     @type string                'name'        The name.
 *     @type string                'in_language' The locale.
 *     @type string                'description' The description.
 *     @type string[]              'same_as'     An array of URLs.
 *     @type string                'logo'        The URL of the logo image.
 *     @type array<string, string> 'publisher' {
 *         @type string $@type The type of the publisher.
 *         @type string $name  The name of the publisher.
 *         @type string $logo  The logo of the publisher.
 *     }
 * }
 */
function the_structured_data( array $args = array() ): void {
	\wpinc\socio\the_structured_data( $args );
}
