<?php
/**
 * Navigation
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2023-11-06
 */

namespace st;

require_once __DIR__ . '/navi/archive.php';
require_once __DIR__ . '/navi/class-nav-menu.php';
require_once __DIR__ . '/navi/link-page-break.php';
require_once __DIR__ . '/navi/page-break.php';
require_once __DIR__ . '/navi/page-hierarchy.php';
require_once __DIR__ . '/navi/post.php';
require_once __DIR__ . '/navi/shortcode.php';

/** phpcs:ignore
 * Constructs a navigation menu.
 *
 * phpcs:ignore
 * @param array{
 *     menu_location    : string,
 *     anchored_page_ids: int[],
 *     object_types     : string[]|string,
 *     home_url         : string,
 *     title_filter     : callable,
 *     content_filter   : callable,
 *     do_echo_group_id : bool
 * } $args An array of arguments.
 *
 * $args {
 *     An array of arguments.
 *
 *     @type string          'menu_location'     Menu location.
 *     @type int[]           'anchored_page_ids' Page ids for making their links of menu items anchor links.
 *     @type string[]|string 'object_types'      Object types.
 *     @type string          'home_url'          Home URL.
 *     @type callable        'title_filter'      Filter function for titles. Default 'esc_html'.
 *     @type callable        'content_filter'    Filter function for contents. Default 'esc_html'.
 *     @type bool            'do_echo_group_id'  Whether to output ID of groups. Default true.
 * }
 * @return \wpinc\navi\Nav_Menu Navigation menu.
 */
function create_nav_menu( array $args ): \wpinc\navi\Nav_Menu {
	return new \wpinc\navi\Nav_Menu( $args );
}

/**
 * Enable cache of navigation menus.
 */
function enable_nav_menu_cache(): void {
	\wpinc\navi\Nav_Menu::enable_cache();
}

/**
 * Adds the archive slug of a custom post type to navigation menus.
 *
 * @param string $post_type Post type.
 * @param string $slug      Slug of the archive page.
 */
function add_custom_post_type_archive_to_nav_menu( string $post_type, string $slug ): void {
	\wpinc\navi\Nav_Menu::add_custom_post_type_archive( $post_type, $slug );
}


// -----------------------------------------------------------------------------


/**
 * Enables next and previous link tags.
 */
function enable_link_page_break(): void {
	\wpinc\navi\enable_link_page_break();
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Displays yearly archive select.
 *
 * phpcs:ignore
 * @param array{
 *     type?        : string,
 *     default_text?: string,
 *     date?        : string,
 *     meta_key?    : string,
 * } $args (Optional) Array of arguments. See get_date_archives() for information on accepted arguments.
 */
function the_yearly_archive_select( array $args = array() ): void {
	\wpinc\navi\the_yearly_archive_select( $args );
}

/** phpcs:ignore
 * Displays taxonomy archive select.
 *
 * phpcs:ignore
 * @param array{
 *     type?        : string,
 *     default_text?: string,
 *     taxonomy?    : string,
 * } $args (Optional) Array of arguments. See get_taxonomy_archives() for information on accepted arguments.
 */
function the_taxonomy_archive_select( array $args = array() ): void {
	\wpinc\navi\the_taxonomy_archive_select( $args );
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Displays date archive links based on type and format.
 *
 * phpcs:ignore
 * @param array{
 *     type?        : string,
 *     default_text?: string,
 *     date?        : string,
 *     meta_key?    : string,
 * } $args (Optional) Array of arguments. See get_date_archives() for information on accepted arguments.
 */
function the_date_archives( array $args = array() ): void {
	\wpinc\navi\the_date_archives( $args );
}

/** phpcs:ignore
 * Retrieves date archive links based on type and format.
 *
 * phpcs:ignore
 * @param array{
 *     before?       : string,
 *     after?        : string,
 *     type?         : string,
 *     link_before?  : string,
 *     link_after?   : string,
 *     do_show_count?: bool,
 *     default_text? : string,
 *     post_type?    : string,
 *     date?         : string,
 *     limit?        : string|int,
 *     order?        : string,
 *     meta_key?     : string,
 * } $args (Optional) Array of type, format, and term query parameters.
 *
 * $args {
 *     (Optional) Array of type, format, and term query parameters.
 *
 *     @type string     'before'        Content to prepend to the output. Default ''.
 *     @type string     'after'         Content to append to the output. Default ''.
 *     @type string     'type'          Link format. Can be 'list' or 'select'. Default 'list'.
 *     @type string     'link_before'   Content to prepend to each link. Default value: ''
 *     @type string     'link_after'    Content to append to each link. Default value: ''
 *     @type bool       'do_show_count' Whether to display the post count alongside the link. Default false.
 *     @type string     'default_text'  Default text used when 'type' is 'select'. Default ''.
 *     @type string     'post_type'     Post type. Default 'post'.
 *     @type string     'date'          Type of archive to retrieve. Accepts 'daily', 'monthly', or 'yearly'. Default 'yearly'.
 *     @type string|int 'limit'         Number of links to limit the query to. Default empty (no limit).
 *     @type string     'order'         Whether to use ascending or descending order. Accepts 'ASC', or 'DESC'. Default 'DESC'.
 *     @type string     'meta_key'      Meta key used instead of post_date.
 * }
 * @return string String of links.
 */
function get_date_archives( array $args = array() ): string {
	return \wpinc\navi\get_date_archives( $args );
}

/** phpcs:ignore
 * Displays taxonomy archive links based on type and format.
 *
 * phpcs:ignore
 * @param array{
 *     type?        : string,
 *     default_text?: string,
 *     taxonomy?    : string,
 * } $args (Optional) Array of arguments. See get_taxonomy_archives() for information on accepted arguments.
 */
function the_taxonomy_archives( array $args = array() ): void {
	\wpinc\navi\the_taxonomy_archives( $args );
}

/** phpcs:ignore
 * Retrieves taxonomy archive links based on type and format.
 *
 * phpcs:ignore
 * @param array{
 *     before?       : string,
 *     after?        : string,
 *     type?         : string,
 *     link_before?  : string,
 *     link_after?   : string,
 *     do_show_count?: bool,
 *     default_text? : string,
 *     post_type?    : string,
 *     taxonomy?     : string,
 *     limit?        : string|int,
 *     order?        : string,
 *     hierarchical? : bool,
 *     parent?       : int,
 * } $args (Optional) Array of type, format, and term query parameters.
 *
 * $args {
 *     (Optional) Array of type, format, and term query parameters.
 *
 *     @type string     'before'        Content to prepend to the output. Default ''.
 *     @type string     'after'         Content to append to the output. Default ''.
 *     @type string     'type'          Link format. Can be 'list' or 'select'. Default 'list'.
 *     @type string     'link_before'   Content to prepend to each link. Default value: ''
 *     @type string     'link_after'    Content to append to each link. Default value: ''
 *     @type bool       'do_show_count' Whether to display the post count alongside the link. Default false.
 *     @type string     'default_text'  Default text used when 'type' is 'select'. Default ''.
 *     @type string     'post_type'     Post type. Default ''.
 *     @type string     'taxonomy'      Taxonomy name to which results should be limited.
 *     @type string|int 'limit'         Number of links to limit the query to. Default empty (no limit).
 *     @type string     'order'         Whether to use ascending or descending order. Accepts 'ASC', or 'DESC'. Default 'DESC'.
 *     @type bool       'hierarchical'  Whether to include terms that have non-empty descendants. Default false.
 *     @type int        'parent'        Parent term ID to retrieve direct-child terms of. Default 0.
 * }
 * @return string String of links.
 */
function get_taxonomy_archives( array $args = array() ): string {
	return \wpinc\navi\get_taxonomy_archives( $args );
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Displays a page break navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     prev_text?         : string,
 *     next_text?         : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     mid_size?          : int,
 *     end_size?          : int,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 * } $args (Optional) Default page break navigation arguments.
 */
function the_page_break_navigation( array $args = array() ): void {
	\wpinc\navi\the_page_break_navigation( $args );
}

/** phpcs:ignore
 * Displays the navigation to page breaks, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     prev_text?         : string,
 *     next_text?         : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     mid_size?          : int,
 *     end_size?          : int,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 * } $args (Optional) Default page break navigation arguments.
 *
 * $args {
 *     (Optional) Default page break navigation arguments.
 *
 *     @type string 'before'             Content to prepend to the output. Default ''.
 *     @type string 'after'              Content to append to the output. Default ''.
 *     @type string 'prev_text'          Anchor text to display in the previous post link. Default ''.
 *     @type string 'next_text'          Anchor text to display in the next post link. Default ''.
 *     @type string 'screen_reader_text' Screen reader text for the nav element. Default 'Post break navigation'.
 *     @type string 'aria_label'         ARIA label text for the nav element. Default 'Page breaks'.
 *     @type string 'class'              Custom class for the nav element. Default 'page-break-navigation'.
 *     @type string 'type'               Link format. Can be 'list', 'select', or custom.
 *     @type int    'mid_size'           How many numbers to either side of the current pages. Default 2.
 *     @type int    'end_size'           How many numbers on either the start and the end list edges. Default 1.
 *     @type string 'link_before'        Content to prepend to each page number. Default ''.
 *     @type string 'link_after'         Content to append to each page number. Default ''.
 *     @type string 'links_before'       Content to prepend to the page numbers. Default ''.
 *     @type string 'links_after'        Content to append to the page numbers. Default ''.
 * }
 * @return string Markup for page break links.
 */
function get_the_page_break_navigation( array $args = array() ): string {
	return \wpinc\navi\get_the_page_break_navigation( $args );
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Displays a child page navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 *     filter?            : callable|null,
 *     post_id?           : int,
 * } $args  (Optional) Default navigation arguments.
 * @param array<string, mixed> $query_args (Optional) Arguments for get_post().
 */
function the_child_page_navigation( array $args = array(), array $query_args = array() ): void {
	\wpinc\navi\the_child_page_navigation( $args, $query_args );
}

/** phpcs:ignore
 * Displays a sibling page navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 *     filter?            : callable|null,
 *     post_id?           : int,
 * } $args (Optional) Default navigation arguments.
 * @param array<string, mixed> $query_args (Optional) Arguments for get_post().
 */
function the_sibling_page_navigation( array $args = array(), array $query_args = array() ): void {
	\wpinc\navi\the_sibling_page_navigation( $args, $query_args );
}

/** phpcs:ignore
 * Retrieves a child page navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 *     filter?            : callable|null,
 *     post_id?           : int,
 * } $args  (Optional) Default navigation arguments.
 * $args {
 *     (Optional) Default navigation arguments.
 *
 *     @type string        'before'             Content to prepend to the output. Default ''.
 *     @type string        'after'              Content to append to the output. Default ''.
 *     @type string        'screen_reader_text' Screen reader text for navigation element. Default 'Child pages navigation'.
 *     @type string        'aria_label'         ARIA label text for the nav element. Default 'Child pages'.
 *     @type string        'class'              Custom class for the nav element. Default 'child-page-navigation'.
 *     @type string        'type'               Link format. Can be 'list', 'select', or custom.
 *     @type string        'link_before'        Content to prepend to each link. Default value: ''
 *     @type string        'link_after'         Content to append to each link. Default value: ''
 *     @type string        'links_before'       Content to prepend to links. Default value: ''
 *     @type string        'links_after'        Content to append to links. Default value: ''
 *     @type callable|null 'filter'             Callback function for filtering. Default null.
 *     @type int           'post_id'            (Optional) Post ID.
 * }
 * @param array<string, mixed> $query_args (Optional) Arguments for get_post().
 * @return string Markup for child page links.
 */
function get_the_child_page_navigation( array $args = array(), array $query_args = array() ): string {
	return \wpinc\navi\get_the_child_page_navigation( $args, $query_args );
}

/** phpcs:ignore
 * Retrieves a sibling page navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 *     filter?            : callable|null,
 *     post_id?           : int,
 * } $args (Optional) Default navigation arguments.
 * $args {
 *     (Optional) Default navigation arguments.
 *
 *     @type string        'before'             Content to prepend to the output. Default ''.
 *     @type string        'after'              Content to append to the output. Default ''.
 *     @type string        'screen_reader_text' Screen reader text for navigation element. Default 'Sibling pages navigation'.
 *     @type string        'aria_label'         ARIA label text for the nav element. Default 'Sibling pages'.
 *     @type string        'class'              Custom class for the nav element. Default 'sibling-page-navigation'.
 *     @type string        'type'               Link format. Can be 'list', 'select', or custom.
 *     @type string        'link_before'        Content to prepend to each link. Default value: ''
 *     @type string        'link_after'         Content to append to each link. Default value: ''
 *     @type string        'links_before'       Content to prepend to links. Default value: ''
 *     @type string        'links_after'        Content to append to links. Default value: ''
 *     @type callable|null 'filter'             Callback function for filtering. Default null.
 *     @type int           'post_id'            (Optional) Post ID.
 * }
 * @param array<string, mixed> $query_args (Optional) Arguments for get_post().
 * @return string Markup for sibling page links.
 */
function get_the_sibling_page_navigation( array $args = array(), array $query_args = array() ): string {
	return \wpinc\navi\get_the_sibling_page_navigation( $args, $query_args );
}


// -----------------------------------------------------------------------------


/** phpcs:ignore
 * Displays a post navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     prev_text?         : string,
 *     next_text?         : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     in_same_term?      : bool,
 *     excluded_terms?    : int[]|string,
 *     taxonomy?          : string,
 *     has_archive_link?  : bool,
 *     archive_text?      : string,
 *     archive_link_pos?  : string,
 * } $args (Optional) Default post navigation arguments.
 */
function the_post_navigation( array $args = array() ): void {
	\wpinc\navi\the_post_navigation( $args );
}

/** phpcs:ignore
 * Displays a posts navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     prev_text?         : string,
 *     next_text?         : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     mid_size?          : string,
 *     end_size?          : string,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 *     add_args?          : string,
 *     add_fragment?      : string,
 * } $args (Optional) Default posts navigation arguments.
 */
function the_posts_navigation( array $args = array() ): void {
	\wpinc\navi\the_posts_navigation( $args );
}

/** phpcs:ignore
 * Retrieves a post navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     prev_text?         : string,
 *     next_text?         : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     in_same_term?      : bool,
 *     excluded_terms?    : int[]|string,
 *     taxonomy?          : string,
 *     has_archive_link?  : bool,
 *     archive_text?      : string,
 *     archive_link_pos?  : string,
 * } $args (Optional) Default post navigation arguments.
 * $args {
 *     (Optional) Default post navigation arguments.
 *
 *     @type string       'before'               Content to prepend to the output. Default ''.
 *     @type string       'after'                Content to append to the output. Default ''.
 *     @type string       'prev_text'            Anchor text to display in the previous post link. Default ''.
 *     @type string       'next_text'            Anchor text to display in the next post link. Default ''.
 *     @type string       'screen_reader_text'   Screen reader text for the nav element. Default 'Post navigation'.
 *     @type string       'aria_label'           ARIA label text for the nav element. Default 'Post'.
 *     @type string       'class'                Custom class for the nav element. Default 'post-navigation'.
 *     @type bool         'in_same_term'         Whether link should be in a same taxonomy term. Default false.
 *     @type int[]|string 'excluded_terms'       Array or comma-separated list of excluded term IDs.
 *     @type string       'taxonomy'             Taxonomy, if 'in_same_term' is true. Default 'category'.
 *     @type bool         'has_archive_link'     Whether the archive link is contained. Default false.
 *     @type string       'archive_text'         Anchor text to display in the archive link. Default 'List'.
 *     @type string       'archive_link_pos'     Position of archive link, if 'has_archive_link' is true. Can be 'start', 'center', or 'end'. Default 'center'.
 * }
 * @return string Markup for post links.
 */
function get_the_post_navigation( array $args = array() ): string {
	return \wpinc\navi\get_the_post_navigation( $args );
}

/** phpcs:ignore
 * Retrieves a posts navigation, when applicable.
 *
 * phpcs:ignore
 * @param array{
 *     before?            : string,
 *     after?             : string,
 *     prev_text?         : string,
 *     next_text?         : string,
 *     screen_reader_text?: string,
 *     aria_label?        : string,
 *     class?             : string,
 *     type?              : string,
 *     mid_size?          : string,
 *     end_size?          : string,
 *     link_before?       : string,
 *     link_after?        : string,
 *     links_before?      : string,
 *     links_after?       : string,
 *     add_args?          : string,
 *     add_fragment?      : string,
 * } $args (Optional) Default posts navigation arguments.
 * $args {
 *     (Optional) Default posts navigation arguments.
 *
 *     @type string 'before'             Content to prepend to the output. Default 'Previous'.
 *     @type string 'after'              Content to append to the output. Default 'Next'.
 *     @type string 'prev_text'          Anchor text to display in the previous post link. Default ''.
 *     @type string 'next_text'          Anchor text to display in the next post link. Default ''.
 *     @type string 'screen_reader_text' Screen reader text for the nav element. Default 'Posts navigation'.
 *     @type string 'aria_label'         ARIA label text for the nav element. Default 'Pages'.
 *     @type string 'class'              Custom class for the nav element. Default 'page-break-navigation'.
 *     @type string 'type'               Link format. Can be 'list', 'select', or custom.
 *     @type string 'mid_size'           How many numbers to either side of the current pages. Default 2.
 *     @type string 'end_size'           How many numbers on either the start and the end list edges. Default 1.
 *     @type string 'link_before'        Content to prepend to each page number. Default ''.
 *     @type string 'link_after'         Content to append to each page number. Default ''.
 *     @type string 'links_before'       Content to prepend to the page numbers. Default ''.
 *     @type string 'links_after'        Content to append to the page numbers. Default ''.
 *     @type string 'add_args'           An array of query args to add.
 *     @type string 'add_fragment'       A string to append to each link.
 * }
 * @return string Markup for posts links.
 */
function get_the_posts_navigation( array $args = array() ): string {
	return \wpinc\navi\get_the_posts_navigation( $args );
}


// -----------------------------------------------------------------------------


/**
 * Adds page navigation shortcodes.
 */
function add_page_navigation_shortcode(): void {
	\wpinc\navi\add_page_navigation_shortcode();
}

/** phpcs:ignore
 * Adds page list shortcode.
 *
 * @param string $post_type Post type.
 * @param string $taxonomy  Taxonomy.
 * phpcs:ignore
 * @param array{
 *     post_type?         : string,
 *     year_date_function?: callable,
 *     before?            : string,
 *     after?             : string,
 *     template_slug?     : string,
 *     heading_level?     : int,
 *     year_heading_level?: int,
 *     year_format?       : string,
 *     taxonomy?          : string,
 *     terms?             : string|string[],
 *     latest?            : int,
 *     sticky?            : bool,
 *     order?             : string,
 *     orderby?           : string,
 *     date_after?        : string,
 *     date_before?       : string,
 * } $args Arguments for get_post_list.
 */
function add_post_list_shortcode( string $post_type, string $taxonomy = '', array $args = array() ): void {
	\wpinc\navi\add_post_list_shortcode( $post_type, $taxonomy, $args );
}
