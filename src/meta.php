<?php
/**
 * Post Meta
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2023-12-28
 */

namespace st\meta {  // phpcs:ignore
	require_once __DIR__ . '/meta/assets/multiple.php';
	require_once __DIR__ . '/meta/field.php';
	require_once __DIR__ . '/meta/field-post.php';
	require_once __DIR__ . '/meta/field-term.php';
	require_once __DIR__ . '/meta/key-suffix.php';
	require_once __DIR__ . '/meta/picker.php';
	require_once __DIR__ . '/meta/utility.php';

	/**
	 * Initializes fields.
	 *
	 * @param array<string, mixed> $args {
	 *     (Optional) Array of arguments.
	 *
	 *     @type string 'url_to' URL to this script.
	 * }
	 */
	function initialize( array $args = array() ): void {
		\wpinc\meta\initialize( $args );
	}

	/**
	 * Adds a separator to post.
	 */
	function add_separator(): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_separator_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_separator_to_post();
	}

	/**
	 * Adds an input to post.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param string $type    Input type. Default 'text'.
	 */
	function add_input( int $post_id, string $key, string $label, string $type = 'text' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_input_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_input_to_post( $post_id, $key, $label, $type );
	}

	/**
	 * Adds a textarea to post.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param int    $rows    Rows attribute. Default 2.
	 */
	function add_textarea( int $post_id, string $key, string $label, int $rows = 2 ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_textarea_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_textarea_to_post( $post_id, $key, $label, $rows );
	}

	/**
	 * Adds a rich editor to post.
	 *
	 * @param int                  $post_id  Post ID.
	 * @param string               $key      Meta key.
	 * @param string               $label    Label.
	 * @param array<string, mixed> $settings Settings for wp_editor.
	 */
	function add_rich_editor( int $post_id, string $key, string $label, array $settings = array() ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_rich_editor_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_rich_editor_to_post( $post_id, $key, $label, $settings );
	}

	/**
	 * Adds a checkbox to post.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param string $title   Title of the checkbox.
	 */
	function add_checkbox( int $post_id, string $key, string $label, string $title = '' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_checkbox_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_checkbox_to_post( $post_id, $key, $label, $title );
	}

	/**
	 * Adds a term select to post.
	 *
	 * @param int    $post_id  Post ID.
	 * @param string $key      Meta key.
	 * @param string $label    Label.
	 * @param string $taxonomy Taxonomy slug.
	 * @param string $field    Term field.
	 */
	function add_term_select( int $post_id, string $key, string $label, string $taxonomy, string $field = 'slug' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_term_select_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_term_select_to_post( $post_id, $key, $label, $taxonomy, $field );
	}

	/**
	 * Adds a related term select to post.
	 *
	 * @param int    $post_id  Post ID.
	 * @param string $key      Meta key.
	 * @param string $label    Label.
	 * @param string $taxonomy Taxonomy slug.
	 * @param string $field    Term field.
	 */
	function add_related_term_select( int $post_id, string $key, string $label, string $taxonomy, string $field = 'slug' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_related_term_select_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_related_term_select_to_post( $post_id, $key, $label, $taxonomy, $field );
	}


	/**
	 * Outputs a separator to post.
	 */
	function output_separator(): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_separator instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_separator();
	}

	/**
	 * Outputs an input row to post.
	 *
	 * @param string $label Label.
	 * @param string $key   Meta key.
	 * @param string $val   Current value.
	 * @param string $type  Input type. Default 'text'.
	 */
	function output_input_row( string $label, string $key, string $val, string $type = 'text' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_input_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_input_row( $label, $key, $val, $type );
	}

	/**
	 * Outputs a textarea row to post.
	 *
	 * @param string $label Label.
	 * @param string $key   Meta key.
	 * @param string $val   Current value.
	 * @param int    $rows  Rows attribute. Default 2.
	 */
	function output_textarea_row( string $label, string $key, string $val, int $rows = 2 ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_textarea_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_textarea_row( $label, $key, $val, $rows );
	}

	/**
	 * Outputs a rich editor row to post.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key.
	 * @param string               $val      Current value.
	 * @param array<string, mixed> $settings Settings for wp_editor.
	 */
	function output_rich_editor_row( string $label, string $key, string $val, array $settings = array() ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_rich_editor_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_rich_editor_row( $label, $key, $val, $settings );
	}

	/**
	 * Outputs a checkbox row to post.
	 *
	 * @param string $label   Label.
	 * @param string $key     Meta key.
	 * @param bool   $checked Current value. Default false.
	 */
	function output_checkbox_row( string $label, string $key, bool $checked = false ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_checkbox_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_checkbox_row( $label, $key, $checked );
	}

	/**
	 * Outputs a term select row to post.
	 *
	 * @param string            $label             Label.
	 * @param string            $key               Meta key.
	 * @param string|\WP_Term[] $taxonomy_or_terms Taxonomy slug or term objects.
	 * @param string            $val               Current value.
	 * @param string            $field             Term field.
	 */
	function output_term_select_row( string $label, string $key, $taxonomy_or_terms, $val, string $field = 'slug' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_term_select_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_term_select_row( $label, $key, $taxonomy_or_terms, $val, $field );
	}

	/**
	 * Adds a separator to post.
	 */
	function add_separator_to_post(): void {
		\wpinc\meta\add_separator_to_post();
	}

	/**
	 * Adds an input to post.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param string $type    Input type. Default 'text'.
	 */
	function add_input_to_post( int $post_id, string $key, string $label, string $type = 'text' ): void {
		\wpinc\meta\add_input_to_post( $post_id, $key, $label, $type );
	}

	/**
	 * Adds a textarea to post.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param int    $rows    Rows attribute. Default 2.
	 */
	function add_textarea_to_post( int $post_id, string $key, string $label, int $rows = 2 ): void {
		\wpinc\meta\add_textarea_to_post( $post_id, $key, $label, $rows );
	}

	/**
	 * Adds a rich editor to post.
	 *
	 * @param int                  $post_id  Post ID.
	 * @param string               $key      Meta key.
	 * @param string               $label    Label.
	 * @param array<string, mixed> $settings Settings for wp_editor.
	 */
	function add_rich_editor_to_post( int $post_id, string $key, string $label, array $settings = array() ): void {
		\wpinc\meta\add_rich_editor_to_post( $post_id, $key, $label, $settings );
	}

	/**
	 * Adds a checkbox to post.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param string $title   Title of the checkbox.
	 */
	function add_checkbox_to_post( int $post_id, string $key, string $label, string $title = '' ): void {
		\wpinc\meta\add_checkbox_to_post( $post_id, $key, $label, $title );
	}

	/**
	 * Adds a post select to post.
	 *
	 * @param int        $post_id Post ID.
	 * @param string     $key     Meta key.
	 * @param string     $label   Label.
	 * @param \WP_Post[] $posts   Posts to be selected.
	 */
	function add_post_select_to_post( int $post_id, string $key, string $label, array $posts ): void {
		\wpinc\meta\add_post_select_to_post( $post_id, $key, $label, $posts );
	}

	/**
	 * Adds a term select to post.
	 *
	 * @param int    $post_id  Post ID.
	 * @param string $key      Meta key.
	 * @param string $label    Label.
	 * @param string $taxonomy Taxonomy slug.
	 * @param string $field    Term field.
	 */
	function add_term_select_to_post( int $post_id, string $key, string $label, string $taxonomy, string $field = 'slug' ): void {
		\wpinc\meta\add_term_select_to_post( $post_id, $key, $label, $taxonomy, $field );
	}

	/**
	 * Adds a related term select to post.
	 *
	 * @param int    $post_id  Post ID.
	 * @param string $key      Meta key.
	 * @param string $label    Label.
	 * @param string $taxonomy Taxonomy slug.
	 * @param string $field    Term field.
	 */
	function add_related_term_select_to_post( int $post_id, string $key, string $label, string $taxonomy, string $field = 'slug' ): void {
		\wpinc\meta\add_related_term_select_to_post( $post_id, $key, $label, $taxonomy, $field );
	}

	/**
	 * Adds a separator to term.
	 */
	function add_separator_to_term(): void {
		\wpinc\meta\add_separator_to_term();
	}

	/**
	 * Adds an input to term.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param string $type    Input type. Default 'text'.
	 */
	function add_input_to_term( int $term_id, string $key, string $label, string $type = 'text' ): void {
		\wpinc\meta\add_input_to_term( $term_id, $key, $label, $type );
	}

	/**
	 * Adds a textarea to term.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param int    $rows    Rows attribute. Default 2.
	 */
	function add_textarea_to_term( int $term_id, string $key, string $label, int $rows = 2 ): void {
		\wpinc\meta\add_textarea_to_term( $term_id, $key, $label, $rows );
	}

	/**
	 * Adds a rich editor to term.
	 *
	 * @param int                  $term_id  Term ID.
	 * @param string               $key      Meta key.
	 * @param string               $label    Label.
	 * @param array<string, mixed> $settings Settings for wp_editor.
	 */
	function add_rich_editor_to_term( int $term_id, string $key, string $label, array $settings = array() ): void {
		\wpinc\meta\add_rich_editor_to_term( $term_id, $key, $label, $settings );
	}

	/**
	 * Adds a checkbox to term.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Meta key.
	 * @param string $label   Label.
	 * @param string $title   Title of the checkbox.
	 */
	function add_checkbox_to_term( int $term_id, string $key, string $label, string $title = '' ): void {
		\wpinc\meta\add_checkbox_to_term( $term_id, $key, $label, $title );
	}

	/**
	 * Adds a post select to term.
	 *
	 * @param int        $term_id Term ID.
	 * @param string     $key     Meta key.
	 * @param string     $label   Label.
	 * @param \WP_Post[] $posts   Posts to be selected.
	 */
	function add_post_select_to_term( int $term_id, string $key, string $label, array $posts ): void {
		\wpinc\meta\add_post_select_to_term( $term_id, $key, $label, $posts );
	}

	/**
	 * Outputs a separator to post.
	 */
	function output_post_separator(): void {
		\wpinc\meta\output_post_separator();
	}

	/**
	 * Outputs an input row to post.
	 *
	 * @param string $label Label.
	 * @param string $key   Meta key.
	 * @param string $val   Current value.
	 * @param string $type  Input type. Default 'text'.
	 */
	function output_post_input_row( string $label, string $key, string $val, string $type = 'text' ): void {
		\wpinc\meta\output_post_input_row( $label, $key, $val, $type );
	}

	/**
	 * Outputs a textarea row to post.
	 *
	 * @param string $label Label.
	 * @param string $key   Meta key.
	 * @param string $val   Current value.
	 * @param int    $rows  Rows attribute. Default 2.
	 */
	function output_post_textarea_row( string $label, string $key, string $val, int $rows = 2 ): void {
		\wpinc\meta\output_post_textarea_row( $label, $key, $val, $rows );
	}

	/**
	 * Outputs a rich editor row to post.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key.
	 * @param string               $val      Current value.
	 * @param array<string, mixed> $settings Settings for wp_editor.
	 */
	function output_post_rich_editor_row( string $label, string $key, string $val, array $settings = array() ): void {
		\wpinc\meta\output_post_rich_editor_row( $label, $key, $val, $settings );
	}

	/**
	 * Outputs a checkbox row to post.
	 *
	 * @param string $label   Label.
	 * @param string $key     Meta key.
	 * @param bool   $checked Current value. Default false.
	 * @param string $title   Title of the checkbox.
	 */
	function output_post_checkbox_row( string $label, string $key, bool $checked = false, string $title = '' ): void {
		\wpinc\meta\output_post_checkbox_row( $label, $key, $checked, $title );
	}

	/**
	 * Outputs a post select row to post.
	 *
	 * @param string     $label Label.
	 * @param string     $key   Meta key.
	 * @param \WP_Post[] $posts Posts to be selected.
	 * @param int        $val   Current value.
	 */
	function output_post_post_select_row( string $label, string $key, array $posts, int $val ): void {
		\wpinc\meta\output_post_post_select_row( $label, $key, $posts, $val );
	}

	/**
	 * Outputs a term select row to post.
	 *
	 * @param string            $label             Label.
	 * @param string            $key               Meta key.
	 * @param string|\WP_Term[] $taxonomy_or_terms Taxonomy slug or term objects.
	 * @param string            $val               Current value.
	 * @param string            $field             Term field.
	 */
	function output_post_term_select_row( string $label, string $key, $taxonomy_or_terms, $val, string $field = 'slug' ): void {
		\wpinc\meta\output_post_term_select_row( $label, $key, $taxonomy_or_terms, $val, $field );
	}

	/**
	 * Outputs a separator to term.
	 */
	function output_term_separator(): void {
		\wpinc\meta\output_term_separator();
	}

	/**
	 * Outputs an input row to term.
	 *
	 * @param string $label Label.
	 * @param string $key   Meta key.
	 * @param string $val   Current value.
	 * @param string $type  Input type. Default 'text'.
	 */
	function output_term_input_row( string $label, string $key, string $val, string $type = 'text' ): void {
		\wpinc\meta\output_term_input_row( $label, $key, $val, $type );
	}

	/**
	 * Outputs a textarea row to term.
	 *
	 * @param string $label Label.
	 * @param string $key   Meta key.
	 * @param string $val   Current value.
	 * @param int    $rows  Rows attribute. Default 2.
	 */
	function output_term_textarea_row( string $label, string $key, string $val, int $rows = 2 ): void {
		\wpinc\meta\output_term_textarea_row( $label, $key, $val, $rows );
	}

	/**
	 * Outputs a rich editor row to term.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key.
	 * @param string               $val      Current value.
	 * @param array<string, mixed> $settings Settings for wp_editor.
	 */
	function output_term_rich_editor_row( string $label, string $key, string $val, array $settings = array() ): void {
		\wpinc\meta\output_term_rich_editor_row( $label, $key, $val, $settings );
	}

	/**
	 * Outputs a checkbox row to term.
	 *
	 * @param string $label   Label.
	 * @param string $key     Meta key.
	 * @param bool   $checked Current value. Default false.
	 * @param string $title   Title of the checkbox.
	 */
	function output_term_checkbox_row( string $label, string $key, bool $checked = false, string $title = '' ): void {
		\wpinc\meta\output_term_checkbox_row( $label, $key, $checked, $title );
	}

	/**
	 * Outputs a post select row to term.
	 *
	 * @param string     $label Label.
	 * @param string     $key   Meta key.
	 * @param \WP_Post[] $posts Posts to be selected.
	 * @param int        $val   Current value.
	 */
	function output_term_post_select_row( string $label, string $key, array $posts, int $val ): void {
		\wpinc\meta\output_term_post_select_row( $label, $key, $posts, $val );
	}


	// -----------------------------------------------------------------------------


	/**
	 * Adds multiple input to post.
	 *
	 * @param int                $post_id  Post ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param string             $label    Label.
	 * @param string             $type     Input type. Default 'text'.
	 */
	function add_input_postfix( int $post_id, string $key, array $suffixes, string $label, string $type = 'text' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_input_suffix_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_input_suffix_to_post( $post_id, $key, $suffixes, $label, $type );
	}

	/**
	 * Adds multiple textarea to post.
	 *
	 * @param int                $post_id  Post ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param string             $label    Label.
	 * @param int                $rows     Rows attribute. Default 2.
	 */
	function add_textarea_postfix( int $post_id, string $key, array $suffixes, string $label, int $rows = 2 ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_textarea_suffix_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_textarea_suffix_to_post( $post_id, $key, $suffixes, $label, $rows );
	}

	/**
	 * Outputs input rows to post.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key base.
	 * @param array<string|null>   $suffixes Meta key suffixes.
	 * @param array<string, mixed> $vals     Current values.
	 * @param string               $type     Input type. Default 'text'.
	 */
	function output_input_row_postfix( string $label, string $key, array $suffixes, array $vals, string $type = 'text' ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_input_row_suffix instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_input_row_suffix( $label, $key, $suffixes, $vals, $type );
	}

	/**
	 * Outputs textarea rows for post.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key base.
	 * @param array<string|null>   $suffixes Meta key suffixes.
	 * @param array<string, mixed> $vals     Current values.
	 * @param int                  $rows     Rows attribute. Default 2.
	 */
	function output_textarea_row_postfix( string $label, string $key, array $suffixes, array $vals, int $rows = 2 ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_textarea_row_suffix instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_textarea_row_suffix( $label, $key, $suffixes, $vals, $rows );
	}

	/**
	 * Adds multiple input to post.
	 *
	 * @param int                $post_id  Post ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param string             $label    Label.
	 * @param string             $type     Input type. Default 'text'.
	 */
	function add_input_suffix_to_post( int $post_id, string $key, array $suffixes, string $label, string $type = 'text' ): void {
		\wpinc\meta\add_input_suffix_to_post( $post_id, $key, $suffixes, $label, $type );
	}

	/**
	 * Adds multiple input to term.
	 *
	 * @param int                $term_id  Term ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param string             $label    Label.
	 * @param string             $type     Input type. Default 'text'.
	 */
	function add_input_suffix_to_term( int $term_id, string $key, array $suffixes, string $label, string $type = 'text' ): void {
		\wpinc\meta\add_input_suffix_to_term( $term_id, $key, $suffixes, $label, $type );
	}

	/**
	 * Adds multiple textarea to post.
	 *
	 * @param int                $post_id  Post ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param string             $label    Label.
	 * @param int                $rows     Rows attribute. Default 2.
	 */
	function add_textarea_suffix_to_post( int $post_id, string $key, array $suffixes, string $label, int $rows = 2 ): void {
		\wpinc\meta\add_textarea_suffix_to_post( $post_id, $key, $suffixes, $label, $rows );
	}

	/**
	 * Adds multiple textarea to term.
	 *
	 * @param int                $term_id  Term ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param string             $label    Label.
	 * @param int                $rows     Rows attribute. Default 5.
	 */
	function add_textarea_suffix_to_term( int $term_id, string $key, array $suffixes, string $label, int $rows = 5 ): void {
		\wpinc\meta\add_textarea_suffix_to_term( $term_id, $key, $suffixes, $label, $rows );
	}

	/**
	 * Outputs input rows to post.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key base.
	 * @param array<string|null>   $suffixes Meta key suffixes.
	 * @param array<string, mixed> $vals     Current values.
	 * @param string               $type     Input type. Default 'text'.
	 */
	function output_post_input_row_suffix( string $label, string $key, array $suffixes, array $vals, string $type = 'text' ): void {
		\wpinc\meta\output_post_input_row_suffix( $label, $key, $suffixes, $vals, $type );
	}

	/**
	 * Outputs input rows to term.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key base.
	 * @param array<string|null>   $suffixes Meta key suffixes.
	 * @param array<string, mixed> $vals     Current values.
	 * @param string               $type     Input type. Default 'text'.
	 */
	function output_term_input_row_suffix( string $label, string $key, array $suffixes, array $vals, string $type = 'text' ): void {
		\wpinc\meta\output_term_input_row_suffix( $label, $key, $suffixes, $vals, $type );
	}

	/**
	 * Outputs textarea rows for post.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key base.
	 * @param array<string|null>   $suffixes Meta key suffixes.
	 * @param array<string, mixed> $vals     Current values.
	 * @param int                  $rows     Rows attribute. Default 2.
	 */
	function output_post_textarea_row_suffix( string $label, string $key, array $suffixes, array $vals, int $rows = 2 ): void {
		\wpinc\meta\output_post_textarea_row_suffix( $label, $key, $suffixes, $vals, $rows );
	}

	/**
	 * Outputs textarea rows for term.
	 *
	 * @param string               $label    Label.
	 * @param string               $key      Meta key base.
	 * @param array<string|null>   $suffixes Meta key suffixes.
	 * @param array<string, mixed> $vals     Current values.
	 * @param int                  $rows     Rows attribute. Default 2.
	 */
	function output_term_textarea_row_suffix( string $label, string $key, array $suffixes, array $vals, int $rows = 5 ): void {
		\wpinc\meta\output_term_textarea_row_suffix( $label, $key, $suffixes, $vals, $rows );
	}


	// -----------------------------------------------------------------------------


	/**
	 * Retrieve multiple post meta from environ variable $_POST.
	 *
	 * @param string   $base_key Base key of variable names.
	 * @param string[] $keys     Keys of variable names.
	 * @return array<string, mixed>[] The meta values.
	 */
	function get_multiple_post_meta_from_env( string $base_key, array $keys ): array {
		return \wpinc\get_multiple_post_meta_from_env( $base_key, $keys );
	}

	/**
	 * Retrieve multiple post meta values.
	 *
	 * @param int         $post_id     Post ID.
	 * @param string      $base_key    Base key of variable names.
	 * @param string[]    $keys        Keys of variable names.
	 * @param string|null $special_key (Optional) Special key.
	 * @return array<string, mixed>[] The meta values.
	 */
	function get_multiple_post_meta( int $post_id, string $base_key, array $keys, ?string $special_key = null ): array {
		return \wpinc\get_multiple_post_meta( $post_id, $base_key, $keys, $special_key );
	}

	/**
	 * Stores multiple post meta values.
	 *
	 * @param int                    $post_id     Post ID.
	 * @param string                 $base_key    Base key of variable names.
	 * @param array<string, mixed>[] $vals        Values.
	 * @param string[]|null          $keys        Keys of variable names.
	 * @param string|null            $special_key (Optional) Special key.
	 */
	function set_multiple_post_meta( int $post_id, string $base_key, array $vals, ?array $keys = null, ?string $special_key = null ): void {
		\wpinc\set_multiple_post_meta( $post_id, $base_key, $vals, $keys, $special_key );
	}


	// -----------------------------------------------------------------------------


	/**
	 * Adds media picker for post meta.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 */
	function add_media_picker( int $post_id, string $key, string $label ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_media_picker_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_media_picker_to_post( $post_id, $key, $label );
	}

	/**
	 * Outputs media picker row.
	 *
	 * @param string $label    Label.
	 * @param string $key      Key.
	 * @param int    $media_id Media ID.
	 */
	function output_media_picker_row( string $label, string $key, int $media_id = 0 ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_media_picker_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_media_picker_row( $label, $key, $media_id );
	}

	/**
	 * Adds date picker for post meta.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 */
	function add_date_picker( int $post_id, string $key, string $label ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use add_date_picker_to_post instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\add_date_picker_to_post( $post_id, $key, $label );
	}

	/**
	 * Outputs date picker row.
	 *
	 * @param string $label Label.
	 * @param string $key   Key.
	 * @param string $val   Value.
	 */
	function output_date_picker_row( string $label, string $key, string $val ): void {
		if ( WP_DEBUG ) {
			trigger_error( 'Use output_post_date_picker_row instead.', E_USER_DEPRECATED );  // phpcs:ignore
		}
		\wpinc\meta\output_post_date_picker_row( $label, $key, $val );
	}

	/**
	 * Adds media picker for post meta
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 */
	function add_media_picker_to_post( int $post_id, string $key, string $label ): void {
		\wpinc\meta\add_media_picker_to_post( $post_id, $key, $label );
	}

	/**
	 * Adds media picker for term meta.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 */
	function add_media_picker_to_term( int $term_id, string $key, string $label ): void {
		\wpinc\meta\add_media_picker_to_term( $term_id, $key, $label );
	}

	/**
	 * Outputs media picker row for post meta.
	 *
	 * @param string $label    Label.
	 * @param string $key      Key.
	 * @param int    $media_id Media ID.
	 */
	function output_post_media_picker_row( string $label, string $key, int $media_id = 0 ): void {
		\wpinc\meta\output_post_media_picker_row( $label, $key, $media_id );
	}

	/**
	 * Outputs media picker row for term.
	 *
	 * @param string $label    Label.
	 * @param string $key      Key.
	 * @param int    $media_id Media ID.
	 */
	function output_term_media_picker_row( string $label, string $key, int $media_id = 0 ): void {
		\wpinc\meta\output_term_media_picker_row( $label, $key, $media_id );
	}

	/**
	 * Adds date picker for post meta.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 */
	function add_date_picker_to_post( int $post_id, string $key, string $label ): void {
		\wpinc\meta\add_date_picker_to_post( $post_id, $key, $label );
	}

	/**
	 * Adds date picker for term meta.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 */
	function add_date_picker_to_term( int $term_id, string $key, string $label ): void {
		\wpinc\meta\add_date_picker_to_term( $term_id, $key, $label );
	}

	/**
	 * Outputs date picker row for post meta.
	 *
	 * @param string $label Label.
	 * @param string $key   Key.
	 * @param string $val   Value.
	 */
	function output_post_date_picker_row( string $label, string $key, string $val ): void {
		\wpinc\meta\output_post_date_picker_row( $label, $key, $val );
	}

	/**
	 * Outputs date picker row for term.
	 *
	 * @param string $label Label.
	 * @param string $key   Key.
	 * @param string $val   Value.
	 */
	function output_term_date_picker_row( string $label, string $key, string $val ): void {
		\wpinc\meta\output_term_date_picker_row( $label, $key, $val );
	}

	/** phpcs:ignore
	 * Adds color picker for post meta.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 * phpcs:ignore
	 * @param array{
	 *     placeholder?: string,
	 *     default?    : string,
	 * } $opts Options.
	 *
	 * $opts {
	 *     Options.
	 *
	 *     @type string 'placeholder' Placeholder of the input. Default ''.
	 *     @type string 'default'     Default color. Default ''.
	 * }
	 */
	function add_color_picker_to_post( int $post_id, string $key, string $label, array $opts = array() ): void {
		\wpinc\meta\add_color_picker_to_post( $post_id, $key, $label, $opts );
	}

	/** phpcs:ignore
	 * Adds color picker for term meta.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 * phpcs:ignore
	 * @param array{
	 *     placeholder?: string,
	 *     default?    : string,
	 * } $opts Options.
	 *
	 * $opts {
	 *     Options.
	 *
	 *     @type string 'placeholder' Placeholder of the input. Default ''.
	 *     @type string 'default'     Default color. Default ''.
	 * }
	 */
	function add_color_picker_to_term( int $term_id, string $key, string $label, array $opts = array() ): void {
		\wpinc\meta\add_color_picker_to_term( $term_id, $key, $label, $opts );
	}

	/**
	 * Outputs color picker row for post.
	 *
	 * @param string               $label Label.
	 * @param string               $key   Key.
	 * @param string               $val   Value.
	 * @param array<string, mixed> $opts  Options.
	 */
	function output_post_color_picker_row( string $label, string $key, string $val, array $opts = array() ): void {
		\wpinc\meta\output_post_color_picker_row( $label, $key, $val, $opts );
	}

	/**
	 * Outputs color picker row for term.
	 *
	 * @param string               $label Label.
	 * @param string               $key   Key.
	 * @param string               $val   Value.
	 * @param array<string, mixed> $opts  Options.
	 */
	function output_term_color_picker_row( string $label, string $key, string $val, array $opts = array() ): void {
		\wpinc\meta\output_term_color_picker_row( $label, $key, $val, $opts );
	}

	/** phpcs:ignore
	 * Adds color hue picker for post meta.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 * phpcs:ignore
	 * @param array{
	 *     placeholder?: string,
	 *     default?    : string,
	 * } $opts Options.
	 *
	 * $opts {
	 *     Options.
	 *
	 *     @type string 'placeholder' Placeholder of the input. Default ''.
	 *     @type string 'default'     Default color. Default ''.
	 * }
	 */
	function add_color_hue_picker_to_post( int $post_id, string $key, string $label, array $opts = array() ): void {
		\wpinc\meta\add_color_hue_picker_to_post( $post_id, $key, $label, $opts );
	}

	/** phpcs:ignore
	 * Adds color hue picker for term meta.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     Key.
	 * @param string $label   Label.
	 * phpcs:ignore
	 * @param array{
	 *     placeholder?: string,
	 *     default?    : string,
	 * } $opts Options.
	 *
	 * $opts {
	 *     Options.
	 *
	 *     @type string 'placeholder' Placeholder of the input. Default ''.
	 *     @type string 'default'     Default color. Default ''.
	 * }
	 */
	function add_color_hue_picker_to_term( int $term_id, string $key, string $label, array $opts = array() ): void {
		\wpinc\meta\add_color_hue_picker_to_term( $term_id, $key, $label, $opts );
	}

	/**
	 * Outputs color hue picker row for post.
	 *
	 * @param string               $label Label.
	 * @param string               $key   Key.
	 * @param string               $val   Value.
	 * @param array<string, mixed> $opts  Options.
	 */
	function output_post_color_hue_picker_row( string $label, string $key, string $val, array $opts = array() ): void {
		\wpinc\meta\output_post_color_hue_picker_row( $label, $key, $val, $opts );
	}

	/**
	 * Outputs color hue picker row for term.
	 *
	 * @param string               $label Label.
	 * @param string               $key   Key.
	 * @param string               $val   Value.
	 * @param array<string, mixed> $opts  Options.
	 */
	function output_term_color_hue_picker_row( string $label, string $key, string $val, array $opts = array() ): void {
		\wpinc\meta\output_term_color_hue_picker_row( $label, $key, $val, $opts );
	}


	// -----------------------------------------------------------------------------


	/**
	 * Retrieves post meta values of keys with suffixes.
	 *
	 * @param int                $post_id  Post ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @return array<string, mixed> Values.
	 */
	function get_post_meta_suffix( int $post_id, string $key, array $suffixes ): array {
		return \wpinc\meta\get_post_meta_suffix( $post_id, $key, $suffixes );
	}

	/**
	 * Retrieves a post meta field as date.
	 *
	 * @param int         $post_id Post ID.
	 * @param string      $key     The meta key to retrieve.
	 * @param string|null $format  Date format.
	 * @return string Date string.
	 */
	function get_post_meta_date( int $post_id, string $key, ?string $format = null ) {
		return \wpinc\meta\get_post_meta_date( $post_id, $key, $format );
	}

	/**
	 * Retrieves a post meta field as multiple lines.
	 *
	 * @param int    $post_id Post ID.
	 * @param string $key     The meta key to retrieve.
	 * @return string[] Lines.
	 */
	function get_post_meta_lines( int $post_id, string $key ): array {
		return \wpinc\meta\get_post_meta_lines( $post_id, $key );
	}

	/**
	 * Retrieves term meta values of keys with suffixes.
	 *
	 * @param int                $term_id  Term ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @return array<string, mixed> Values.
	 */
	function get_term_meta_suffix( int $term_id, string $key, array $suffixes ): array {
		return \wpinc\meta\get_term_meta_suffix( $term_id, $key, $suffixes );
	}

	/**
	 * Retrieves a term meta field as date.
	 *
	 * @param int         $term_id Term ID.
	 * @param string      $key     The meta key to retrieve.
	 * @param string|null $format  Date format.
	 * @return string Date string.
	 */
	function get_term_meta_date( int $term_id, string $key, ?string $format = null ) {
		return \wpinc\meta\get_term_meta_date( $term_id, $key, $format );
	}

	/**
	 * Retrieves a term meta field as multiple lines.
	 *
	 * @param int    $term_id Term ID.
	 * @param string $key     The meta key to retrieve.
	 * @return string[] Lines.
	 */
	function get_term_meta_lines( int $term_id, string $key ): array {
		return \wpinc\meta\get_term_meta_lines( $term_id, $key );
	}

	/**
	 * Stores a post meta field.
	 *
	 * @param int           $post_id Post ID.
	 * @param string        $key     Metadata key.
	 * @param callable|null $filter  Filter.
	 * @param mixed|null    $def     Default value.
	 */
	function set_post_meta( int $post_id, string $key, ?callable $filter = null, $def = null ): void {
		\wpinc\meta\set_post_meta( $post_id, $key, $filter, $def );
	}

	/**
	 * Stores a post meta field after applying filters.
	 *
	 * @param int         $post_id     Post ID.
	 * @param string      $key         Metadata key.
	 * @param string|null $filter_name Filter name.
	 * @param mixed|null  $def         Default value.
	 */
	function set_post_meta_with_wp_filter( int $post_id, string $key, ?string $filter_name = null, $def = null ): void {
		\wpinc\meta\set_post_meta_with_wp_filter( $post_id, $key, $filter_name, $def );
	}

	/**
	 * Stores post meta values of keys with suffixes.
	 *
	 * @param int                $post_id  Post ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param callable|null      $filter   Filter function.
	 */
	function set_post_meta_suffix( int $post_id, string $key, array $suffixes, ?callable $filter = null ): void {
		\wpinc\meta\set_post_meta_suffix( $post_id, $key, $suffixes, $filter );
	}

	/**
	 * Stores a term meta field.
	 *
	 * @param int           $term_id Term ID.
	 * @param string        $key     Metadata key.
	 * @param callable|null $filter  Filter.
	 * @param mixed|null    $def     Default value.
	 */
	function set_term_meta( int $term_id, string $key, ?callable $filter = null, $def = null ): void {
		\wpinc\meta\set_term_meta( $term_id, $key, $filter, $def );
	}

	/**
	 * Stores a term meta field after applying filters.
	 *
	 * @param int         $term_id     Term ID.
	 * @param string      $key         Metadata key.
	 * @param string|null $filter_name Filter name.
	 * @param mixed|null  $def         Default value.
	 */
	function set_term_meta_with_wp_filter( int $term_id, string $key, ?string $filter_name = null, $def = null ): void {
		\wpinc\meta\set_term_meta_with_wp_filter( $term_id, $key, $filter_name, $def );
	}

	/**
	 * Stores term meta values of keys with suffixes.
	 *
	 * @param int                $term_id  Term ID.
	 * @param string             $key      Meta key base.
	 * @param array<string|null> $suffixes Meta key suffixes.
	 * @param callable|null      $filter   Filter function.
	 */
	function set_term_meta_suffix( int $term_id, string $key, array $suffixes, ?callable $filter = null ): void {
		\wpinc\meta\set_term_meta_suffix( $term_id, $key, $suffixes, $filter );
	}

	/**
	 * Trims string.
	 *
	 * @param string $str String.
	 * @return string Trimmed string.
	 */
	function mb_trim( string $str ): string {
		return \wpinc\meta\mb_trim( $str );
	}

	/**
	 * Outputs name and id attributes.
	 *
	 * @param string $key Key.
	 */
	function name_id( string $key ): void {
		\wpinc\meta\name_id( $key );
	}

	/**
	 * Normalizes date string.
	 *
	 * @param string $str String representing date.
	 * @return string Normalized string.
	 */
	function normalize_date( string $str ): string {
		return \wpinc\meta\normalize_date( $str );
	}

	/**
	 * Normalizes YouTube video ID.
	 *
	 * @param string $str String of YouTube video ID.
	 * @return string Normalized YouTube video ID.
	 */
	function normalize_youtube_video_id( string $str ): string {
		return \wpinc\meta\normalize_youtube_video_id( $str );
	}
}

namespace st\post_term_meta {  // phpcs:ignore
	require_once __DIR__ . '/meta/post-term-meta.php';
	/**
	 * Initialize post term meta.
	 *
	 * @param array<string, mixed> $args {
	 *     Arguments.
	 *
	 *     @type string 'base_meta_key' Base meta key. Default '_pmk'.
	 * }
	 */
	function initialize( array $args = array() ): void {
		\wpinc\meta\post_term_meta\initialize( $args );
	}

	/**
	 * Makes post term meta key.
	 *
	 * @param int    $term_id  Term ID.
	 * @param string $meta_key Meta key.
	 * @return string Post term meta key.
	 */
	function get_key( int $term_id, string $meta_key ): string {
		return \wpinc\meta\post_term_meta\get_key( $term_id, $meta_key );
	}

	/**
	 * Adds a post term meta field.
	 *
	 * @param int    $post_id    Post ID.
	 * @param int    $term_id    Term ID.
	 * @param string $meta_key   Meta key.
	 * @param mixed  $meta_value Metadata value. Must be serializable if non-scalar.
	 * @param bool   $unique     (Optional) Whether the same key should not be added.
	 * @return int|false Meta ID on success, false on failure.
	 */
	function add_post_term_meta( int $post_id, int $term_id, string $meta_key, $meta_value, bool $unique = false ) {
		return \wpinc\meta\post_term_meta\add_post_term_meta( $post_id, $term_id, $meta_key, $meta_value, $unique );
	}

	/**
	 * Deletes a post term meta field.
	 *
	 * @param int    $post_id    Post ID.
	 * @param int    $term_id    Term ID.
	 * @param string $meta_key   Meta key.
	 * @param string $meta_value (Optional) Metadata value. Must be serializable if non-scalar.
	 * @return bool True on success, false on failure.
	 */
	function delete_post_term_meta( int $post_id, int $term_id, string $meta_key, $meta_value = '' ): bool {
		return \wpinc\meta\post_term_meta\delete_post_term_meta( $post_id, $term_id, $meta_key, $meta_value );
	}

	/**
	 * Retrieves a post term meta field.
	 *
	 * @param int    $post_id  Post ID.
	 * @param int    $term_id  Term ID.
	 * @param string $meta_key Meta key.
	 * @param bool   $single   (Optional) Whether to return a single value.
	 * @return mixed An array of values if $single is false. The value of the meta field if $single is true. False for an invalid $post_id (non-numeric, zero, or negative value). An empty string if a valid but non-existing post ID is passed.
	 */
	function get_post_term_meta( int $post_id, int $term_id, string $meta_key, bool $single = false ) {
		return \wpinc\meta\post_term_meta\get_post_term_meta( $post_id, $term_id, $meta_key, $single );
	}

	/**
	 * Updates a post term meta field.
	 *
	 * @param int    $post_id    Post ID.
	 * @param int    $term_id    Term ID.
	 * @param string $meta_key   Meta key.
	 * @param mixed  $meta_value Metadata value. Must be serializable if non-scalar.
	 * @param mixed  $prev_value (Optional) Previous value to check before updating.
	 * @return int|bool Meta ID if the key didn't exist, true on successful update, false on failure or if the value passed to the function is the same as the one that is already in the database.
	 */
	function update_post_term_meta( int $post_id, int $term_id, string $meta_key, $meta_value, $prev_value = '' ) {
		return \wpinc\meta\post_term_meta\update_post_term_meta( $post_id, $term_id, $meta_key, $meta_value, $prev_value );
	}
}
