<?php
/**
 * Dialog
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2024-03-21
 */

namespace st\duration_picker {  // phpcs:ignore
	require_once __DIR__ . '/dia/duration-picker.php';

	/** phpcs:ignore
	 * Initializes duration picker.
	 * phpcs:ignore
	 * @param array{
	 *     key         : non-empty-string,
	 *     url_to?     : string,
	 *     do_autofill?: bool,
	 *     label_from? : string,
	 *     label_to?   : string,
	 *     locale?     : string,
	 * } $args An array of arguments.
	 *
	 * $args {
	 *     An array of arguments.
	 *
	 *     @type string 'key'         Meta key.
	 *     @type string 'url_to'      URL to this script.
	 *     @type bool   'do_autofill' Whether to do autofill.
	 *     @type string 'label_from'  Label for duration 'from'.
	 *     @type string 'label_to'    Label for duration 'to'.
	 *     @type string 'locale'      Locale.
	 * }
	 */
	function initialize( array $args ): void {
		\wpinc\dia\duration_picker\initialize( $args );
	}

	/** phpcs:ignore
	 * Retrieves duration data.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key         : non-empty-string,
	 *     url_to?     : string,
	 *     do_autofill?: bool,
	 *     label_from? : string,
	 *     label_to?   : string,
	 *     locale?     : string,
	 * } $args An array of arguments.
	 * @param int    $post_id (Optional) Post ID.
	 * @return array{ from: string|null, to: string|null }|null Duration data.
	 */
	function get_data( array $args, int $post_id = 0 ): ?array {
		return \wpinc\dia\duration_picker\get_data( $args, $post_id );
	}

	/** phpcs:ignore
	 * Adds the meta box to template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key         : non-empty-string,
	 *     url_to?     : string,
	 *     do_autofill?: bool,
	 *     label_from? : string,
	 *     label_to?   : string,
	 *     locale?     : string,
	 * } $args An array of arguments.
	 * @param string                        $title    Title of the meta box.
	 * @param ?string                       $screen   (Optional) The screen or screens on which to show the box.
	 * @param 'advanced'|'normal'|'side'    $context  (Optional) The context within the screen where the box should display.
	 * @param 'core'|'default'|'high'|'low' $priority (Optional) The priority within the context where the box should show.
	 */
	function add_meta_box( array $args, string $title, ?string $screen = null, string $context = 'side', string $priority = 'default' ): void {
		\wpinc\dia\duration_picker\add_meta_box( $args, $title, $screen, $context, $priority );
	}

	/** phpcs:ignore
	 * Stores the data of the meta box on template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key         : non-empty-string,
	 *     url_to?     : string,
	 *     do_autofill?: bool,
	 *     label_from? : string,
	 *     label_to?   : string,
	 *     locale?     : string,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function save_meta_box( array $args, int $post_id ): void {
		\wpinc\dia\duration_picker\save_meta_box( $args, $post_id );
	}
}

namespace st\link_picker {  // phpcs:ignore
	require_once __DIR__ . '/dia/link-picker.php';

	/** phpcs:ignore
	 * Initializes link picker.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key               : non-empty-string,
	 *     url_to?           : string,
	 *     do_allow_url_hash?: bool,
	 *     internal_only?    : bool,
	 *     max_count?        : int|null,
	 *     post_type?        : string|string[],
	 *     message_label?    : string,
	 * } $args An array of arguments.
	 *
	 * $args {
	 *     An array of arguments.
	 *
	 *     @type string          'key'               Meta key.
	 *     @type string          'url_to'            URL to this script.
	 *     @type bool            'do_allow_url_hash' Whether to allow URL with hash. Default false.
	 *     @type bool            'internal_only'     Whether to limit the target to internal URLs. Default false.
	 *     @type int|null        'max_count'         Maximum count. Default null.
	 *     @type string|string[] 'post_type'         Post types. Default ''.
	 *     @type string          'message_label'     Message label. Default ''.
	 * }
	 */
	function initialize( array $args ): void {
		\wpinc\dia\link_picker\initialize( $args );
	}

	/** phpcs:ignore
	 * Retrieves the link data.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key               : non-empty-string,
	 *     url_to?           : string,
	 *     do_allow_url_hash?: bool,
	 *     internal_only?    : bool,
	 *     max_count?        : int|null,
	 *     post_type?        : string|string[],
	 *     message_label?    : string,
	 * } $args An array of arguments.
	 * @param int    $post_id (Optional) Post ID.
	 * @return array{
	 *     url    : string,
	 *     title  : string,
	 *     post_id: int,
	 * }[] Media data.
	 */
	function get_data( array $args, int $post_id = 0 ): array {
		return \wpinc\dia\link_picker\get_data( $args, $post_id );
	}

	/** phpcs:ignore
	 * Retrieves the posts of the links.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key               : non-empty-string,
	 *     url_to?           : string,
	 *     do_allow_url_hash?: bool,
	 *     internal_only?    : bool,
	 *     max_count?        : int|null,
	 *     post_type?        : string|string[],
	 *     message_label?    : string,
	 * } $args An array of arguments.
	 * @param int    $post_id          (Optional) Post ID.
	 * @param bool   $skip_except_post (Optional) Whether to skip links except posts. Default true.
	 * @return array<\WP_Post|null> Posts.
	 */
	function get_posts( array $args, int $post_id = 0, bool $skip_except_post = true ): array {
		return \wpinc\dia\link_picker\get_posts( $args, $post_id, $skip_except_post );
	}

	/** phpcs:ignore
	 * Adds the meta box to template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key               : non-empty-string,
	 *     url_to?           : string,
	 *     do_allow_url_hash?: bool,
	 *     internal_only?    : bool,
	 *     max_count?        : int|null,
	 *     post_type?        : string|string[],
	 *     message_label?    : string,
	 * } $args An array of arguments.
	 * @param string                        $title    Title of the meta box.
	 * @param ?string                       $screen   (Optional) The screen or screens on which to show the box.
	 * @param 'advanced'|'normal'|'side'    $context  (Optional) The context within the screen where the box should display.
	 * @param 'core'|'default'|'high'|'low' $priority (Optional) The priority within the context where the box should show.
	 */
	function add_meta_box( array $args, string $title, ?string $screen = null, string $context = 'advanced', string $priority = 'default' ): void {
		\wpinc\dia\link_picker\add_meta_box( $args, $title, $screen, $context, $priority );
	}

	/** phpcs:ignore
	 * Stores the data of the meta box on template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key               : non-empty-string,
	 *     url_to?           : string,
	 *     do_allow_url_hash?: bool,
	 *     internal_only?    : bool,
	 *     max_count?        : int|null,
	 *     post_type?        : string|string[],
	 *     message_label?    : string,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function save_meta_box( array $args, int $post_id ): void {
		\wpinc\dia\link_picker\save_meta_box( $args, $post_id );
	}
}

namespace st\media_picker {  // phpcs:ignore
	require_once __DIR__ . '/dia/media-picker.php';

	/** phpcs:ignore
	 * Initializes media picker.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 *
	 * $args {
	 *     An array of arguments.
	 *
	 *     @type string 'key'            Meta key.
	 *     @type string 'url_to'         URL to this script.
	 *     @type string 'title_editable' Whether the title is editable.
	 * }
	 */
	function initialize( array $args ): void {
		\wpinc\dia\media_picker\initialize( $args );
	}

	/** phpcs:ignore
	 * Retrieves the media data.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 * @param int    $post_id (Optional) Post ID.
	 * @return array{
	 *     url     : string,
	 *     title   : string,
	 *     filename: string,
	 *     media_id: int,
	 * }[] Media data.
	 */
	function get_data( array $args, int $post_id = 0 ): array {
		return \wpinc\dia\media_picker\get_data( $args, $post_id );
	}

	/** phpcs:ignore
	 * Adds the meta box to template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 * @param string                        $title    Title of the meta box.
	 * @param ?string                       $screen   (Optional) The screen or screens on which to show the box.
	 * @param 'advanced'|'normal'|'side'    $context  (Optional) The context within the screen where the box should display.
	 * @param 'core'|'default'|'high'|'low' $priority (Optional) The priority within the context where the box should show.
	 */
	function add_meta_box( array $args, string $title, ?string $screen = null, string $context = 'advanced', string $priority = 'default' ): void {
		\wpinc\dia\media_picker\add_meta_box( $args, $title, $screen, $context, $priority );
	}

	/** phpcs:ignore
	 * Stores the data of the meta box on template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An Array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function save_meta_box( array $args, int $post_id ): void {
		\wpinc\dia\media_picker\save_meta_box( $args, $post_id );
	}

	/** phpcs:ignore
	 * Displays the inside of the metabox.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key           : non-empty-string,
	 *     url_to?       : string,
	 *     title_editable: bool,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function output_html( array $args, int $post_id = 0 ): void {
		\wpinc\dia\media_picker\output_html( $args, $post_id );
	}
}

namespace st\post_thumbnail {  // phpcs:ignore
	require_once __DIR__ . '/dia/post-thumbnail.php';

	/** phpcs:ignore
	 * Initializes post thumbnail picker.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     url_to?: string,
	 *     key?   : string,
	 * } $args (Optional) An array of arguments.
	 *
	 * $args {
	 *     (Optional) An array of arguments.
	 *
	 *     @type string 'url_to' URL to this script.
	 *     @type string 'key'    Meta key.
	 * }
	 */
	function initialize( array $args = array() ): void {
		\wpinc\dia\post_thumbnail\initialize( $args );
	}

	/** phpcs:ignore
	 * Retrieves post thumbnail data.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key    : non-empty-string,
	 *     url_to?: string,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 * @return int|null Media ID.
	 */
	function get_data( array $args, int $post_id = 0 ): ?int {
		return \wpinc\dia\post_thumbnail\get_data( $args, $post_id );
	}

	/** phpcs:ignore
	 * Adds the meta box to template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key    : non-empty-string,
	 *     url_to?: string,
	 * } $args An array of arguments.
	 * @param string                        $title    Title of the meta box.
	 * @param ?string                       $screen   (Optional) The screen or screens on which to show the box.
	 * @param 'advanced'|'normal'|'side'    $context  (Optional) The context within the screen where the box should display.
	 * @param 'core'|'default'|'high'|'low' $priority (Optional) The priority within the context where the box should show.
	 */
	function add_meta_box( array $args, string $title, ?string $screen = null, string $context = 'side', string $priority = 'default' ): void {
		\wpinc\dia\post_thumbnail\add_meta_box( $args, $title, $screen, $context, $priority );
	}

	/** phpcs:ignore
	 * Stores the data of the meta box on template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key    : non-empty-string,
	 *     url_to?: string,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function save_meta_box( array $args, int $post_id ): void {
		\wpinc\dia\post_thumbnail\save_meta_box( $args, $post_id );
	}
}

namespace st\rich_editor {  // phpcs:ignore
	require_once __DIR__ . '/dia/rich-editor.php';

	/** phpcs:ignore
	 * Adds the meta box to template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key                : non-empty-string,
	 *     type?              : string,
	 *     editor_option?     : array<string, mixed>,
	 *     key_suffix_title?  : string,
	 *     key_suffix_content?: string,
	 *     label_title?       : string,
	 * } $args An array of arguments.
	 * @param string                        $title    Title of the meta box.
	 * @param ?string                       $screen   (Optional) The screen or screens on which to show the box.
	 * @param 'advanced'|'normal'|'side'    $context  (Optional) The context within the screen where the box should display.
	 * @param 'core'|'default'|'high'|'low' $priority (Optional) The priority within the context where the box should show.
	 */
	function add_meta_box( array $args, string $title, ?string $screen = null, string $context = 'advanced', string $priority = 'default' ): void {
		\wpinc\dia\rich_editor\add_meta_box( $args, $title, $screen, $context, $priority );
	}

	/** phpcs:ignore
	 * Stores the data of the meta box on template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key                : non-empty-string,
	 *     type?              : string,
	 *     editor_option?     : array<string, mixed>,
	 *     key_suffix_title?  : string,
	 *     key_suffix_content?: string,
	 *     label_title?       : string,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function save_meta_box( array $args, int $post_id ): void {
		\wpinc\dia\rich_editor\save_meta_box( $args, $post_id );
	}
}

namespace st\single_media_picker {  // phpcs:ignore
	require_once __DIR__ . '/dia/single-media-picker.php';

	/** phpcs:ignore
	 * Initializes single media picker.
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 *
	 * $args {
	 *     (Optional) An array of arguments.
	 *
	 *     @type string 'key'            Meta key.
	 *     @type string 'url_to'         URL to this script.
	 *     @type bool   'title_editable' Whether the title is editable.
	 * }
	 */
	function initialize( array $args ): void {
		\wpinc\dia\single_media_picker\initialize( $args );
	}

	/** phpcs:ignore
	 * Retrieves the media data.
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 * @param int    $post_id (Optional) Post ID.
	 * @return array{
	 *     url     : string,
	 *     title   : string,
	 *     filename: string,
	 *     media_id: int,
	 * }|null Media data.
	 */
	function get_data( array $args, int $post_id = 0 ): ?array {
		return \wpinc\dia\single_media_picker\get_data( $args, $post_id );
	}

	/** phpcs:ignore
	 * Adds the meta box to template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 * @param string                        $title    Title of the meta box.
	 * @param string|null                   $screen   (Optional) The screen or screens on which to show the box.
	 * @param 'advanced'|'normal'|'side'    $context  (Optional) The context within the screen where the box should display.
	 * @param 'core'|'default'|'high'|'low' $priority (Optional) The priority within the context where the box should show.
	 */
	function add_meta_box( array $args, string $title, ?string $screen = null, string $context = 'advanced', string $priority = 'default' ): void {
		\wpinc\dia\single_media_picker\add_meta_box( $args, $title, $screen, $context, $priority );
	}

	/** phpcs:ignore
	 * Stores the data of the meta box on template admin screen.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key            : non-empty-string,
	 *     url_to?        : string,
	 *     title_editable?: bool,
	 * } $args An array of arguments.
	 * @param int    $post_id Post ID.
	 */
	function save_meta_box( array $args, int $post_id ): void {
		\wpinc\dia\single_media_picker\save_meta_box( $args, $post_id );
	}
}
