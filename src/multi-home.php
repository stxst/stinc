<?php
/**
 * Multi-Home
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2024-03-21
 */

namespace st;

require_once __DIR__ . '/plex/custom-rewrite.php';
require_once __DIR__ . '/plex/pseudo-front.php';
require_once __DIR__ . '/plex/filter.php';
require_once __DIR__ . '/plex/term-field.php';
require_once __DIR__ . '/plex/option-field.php';
require_once __DIR__ . '/plex/post-field.php';
require_once __DIR__ . '/plex/page-for-posts.php';
require_once __DIR__ . '/plex/location.php';

const QUERY_VAR_SITE_HOME = 'site_home';
const TAXONOMY_POST_HOME  = 'post_home';

/** phpcs:ignore
 * Initialize multi-home features.
 *
 * @psalm-suppress ArgumentTypeCoercion
 * phpcs:ignore
 * @param array{
 *     site_homes?                 : string[],
 *     default_home?               : string,
 *     default_home_omittable?     : bool,
 *     admin_labels?               : array<string, string>,
 *     filter_term_labels?         : array<string, string>,
 *     filtered_post_types?        : string[],
 *     multiplexed?                : array{ post_types?: string[], editor_type?: string } ,
 *     do_set_page_on_front_option?: bool,
 *     do_multiplex_nav_menu?      : bool,
 *     do_multiplex_sidebar?       : bool,
 * } $args Configuration arguments.
 * $args {
 *     Configuration arguments.
 *
 *     @type array  'site_homes'
 *     @type string 'default_home'
 *     @type bool   'default_home_omittable'
 *     @type array  'admin_labels'
 *     @type array  'filter_term_labels'
 *     @type array  'filtered_post_types'
 *     @type array  'multiplexed'
 *     @type bool   'do_set_page_on_front_option'
 *     @type bool   'do_multiplex_nav_menu'
 *     @type bool   'do_multiplex_sidebar'
 * }
 */
function initialize_multi_home( array $args ): void {
	$args += array(
		'site_homes'                  => array(),
		'default_home'                => '',
		'default_home_omittable'      => false,
		'admin_labels'                => array(),
		'filter_term_labels'          => array(),
		'filtered_post_types'         => array(),
		'multiplexed'                 => array(),
		'do_set_page_on_front_option' => true,
		'do_multiplex_nav_menu'       => false,
		'do_multiplex_sidebar'        => false,
	);
	$inst  = &_get_multi_home_instance();
	$inst += $args;

	if ( ! empty( $args['site_homes'] ) ) {
		/*
		 * For enabling custom rewrite.
		 */
		\wpinc\plex\custom_rewrite\add_structure(
			array(
				'var'          => QUERY_VAR_SITE_HOME,
				'slugs'        => $args['site_homes'],
				'default_slug' => $args['default_home'],
				'omittable'    => $args['default_home_omittable'],
				'global'       => true,
			)
		);
		\wpinc\plex\custom_rewrite\activate();
	}

	/*
	 * For enabling pseudo front pages.
	 */
	\wpinc\plex\pseudo_front\activate(
		array(
			'has_default_front_bloginfo'  => false,
			'do_set_page_on_front_option' => $args['do_set_page_on_front_option'],
		)
	);
	\wpinc\plex\pseudo_front\add_admin_labels( $args['admin_labels'] );

	/*
	 * For adding filter taxonomy and its terms.
	 */
	\wpinc\plex\filter\activate();
	add_action( 'init', '\st\_cb_init_multi_home', 10, 0 );  // Do here because locale is used.

	/*
	 * For adding post types with multiple homes.
	 */
	if ( ! empty( $args['multiplexed'] ) && isset( $args['multiplexed']['post_types'] ) ) {
		$post_types = $args['multiplexed']['post_types'];
		if ( ! empty( $post_types ) ) {
			\wpinc\plex\post_field\add_admin_labels( $args['admin_labels'] );
			\wpinc\plex\post_field\add_post_type( $post_types );

			$editor_type = $args['multiplexed']['editor_type'] ?? null;
			if ( is_string( $editor_type ) && '' !== $editor_type ) {
				\wpinc\plex\post_field\activate( array( 'editor_type' => $editor_type ) );
			} else {
				\wpinc\plex\post_field\activate();
			}
		}
	}
	// For backward compatibility.
	/** @psalm-suppress InvalidArrayOffset */  // phpcs:ignore
	if ( isset( $args['multiplexed_post_types'] ) && ! empty( $args['multiplexed_post_types'] ) ) {  // @phpstan-ignore-line
		\wpinc\plex\post_field\add_post_type( $args['multiplexed_post_types'] );
		\wpinc\plex\post_field\add_admin_labels( $args['admin_labels'] );
		$a = array(
			'content_key_prefix' => '_post_field_',
		);
		if ( isset( $args['multiplexed_editor_type'] ) ) {  // @phpstan-ignore-line
			$a['editor_type'] = $args['multiplexed_editor_type'];
		}
		\wpinc\plex\post_field\activate( $a );
	}

	/*
	 * For multiplexing 'page_for_posts'.
	 */
	\wpinc\plex\page_for_posts\activate();

	/*
	 * For multiplexing nav menus and sidebars.
	 */
	if ( $args['do_multiplex_nav_menu'] || $args['do_multiplex_sidebar'] ) {
		\wpinc\plex\location\add_admin_labels( $args['admin_labels'] );
		\wpinc\plex\location\activate(
			array(
				'do_multiplex_nav_menu' => $args['do_multiplex_nav_menu'],
				'do_multiplex_sidebar'  => $args['do_multiplex_sidebar'],
			)
		);
	}
}

/**
 * Get instance.
 *
 * @access private
 *
 * @return array{
 *     admin_labels         : array<string, string>,
 *     filter_term_labels   : array<string, string>,
 *     filtered_post_types  : string[],
 *     translated_taxonomies: string[],
 * } Instance.
 */
function &_get_multi_home_instance(): array {
	static $values = array();
	return $values;
}


// -----------------------------------------------------------------------------


/**
 * Callback function for 'init' action.
 *
 * @access private
 * @psalm-suppress ArgumentTypeCoercion
 */
function _cb_init_multi_home(): void {
	$inst = &_get_multi_home_instance();
	\wpinc\plex\filter\add_filter_taxonomy(
		QUERY_VAR_SITE_HOME,
		array(
			'taxonomy'      => TAXONOMY_POST_HOME,
			'slug_to_label' => $inst['filter_term_labels'],
			'label'         => $inst['admin_labels'][ TAXONOMY_POST_HOME ] ?? __( 'Homes', 'theme' ),
		)
	);
	\wpinc\plex\filter\add_filtered_post_type( $inst['filtered_post_types'] );
}


// -----------------------------------------------------------------------------


/**
 * Retrieve current site home.
 *
 * @return string Slugs of the query variable.
 */
function get_site_home(): string {
	return \wpinc\plex\custom_rewrite\get_query_var( QUERY_VAR_SITE_HOME );
}

/**
 * Retrieve home url based on the current site home.
 *
 * @param string   $path Path.
 * @param string[] $vars (Optional) An array of variable name to slug.
 * @return string The home url.
 */
function home_url( string $path = '', array $vars = array() ): string {
	return \wpinc\plex\pseudo_front\home_url( $path, null, $vars );
}

/**
 * Retrieves invalid pagename.
 *
 * @return array<string|null>|null Invalid pagename.
 */
function get_invalid_pagename(): ?array {
	return \wpinc\plex\custom_rewrite\get_invalid_pagename();
}
