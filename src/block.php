<?php
/**
 * Block
 *
 * @package Stinc
 * @author Space-Time Inc.
 * @version 2024-03-01
 */

namespace st\block {  // phpcs:ignore
	require_once __DIR__ . '/blok/custom-block.php';
	require_once __DIR__ . '/blok/field-block.php';
	require_once __DIR__ . '/blok/input-block.php';
	require_once __DIR__ . '/blok/unregister.php';
	require_once __DIR__ . '/blok/util.php';

	/**
	 * Registers custom blocks.
	 *
	 * @param array<string, mixed> $args {
	 *     Arguments.
	 *
	 *     @type string 'category_title' Title of added category.
	 *     @type array  'block-cards' {
	 *         Arguments for cards block.
	 *
	 *         @type string 'class_card' CSS class for card block. Default 'card-%d'.
	 *     }
	 *     @type array  'block-frame' {
	 *         Arguments for frame block.
	 *
	 *         @type string 'class_frame_normal' CSS class for normal frame. Default 'frame'.
	 *         @type string 'class_frame_alt'    CSS class for alt. frame. Default 'frame-alt'.
	 *     }
	 *     @type array  'block-tabs' {
	 *         Arguments for tabs block.
	 *
	 *         @type string 'class_tab_scroll' CSS class for tab scroll. Default 'tab-scroll',
	 *         @type string 'class_tab_stack'  CSS class for tab stack. Default 'tab-stack',
	 *     }
	 * }
	 */
	function register_custom_blocks( array $args = array() ): void {
		\wpinc\blok\register_custom_blocks( $args );
	}

	/**
	 * Registers custom styles.
	 */
	function register_custom_styles(): void {
		\wpinc\blok\register_custom_styles();
	}


	// -------------------------------------------------------------------------


	/** phpcs:ignore
	 * Adds field block.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key                : string,
	 *     label              : string,
	 *     post_type          : string,
	 *     do_render?         : bool,
	 *     do_support_classic?: bool,
	 * } $args An array of arguments.
	 *
	 * $args {
	 *     An array of arguments.
	 *
	 *     @type string 'key'                Key of post meta.
	 *     @type string 'label'              Label of the post meta.
	 *     @type string 'post_type'          Target post type.
	 *     @type bool   'do_render'          Whether to render before storing contents.
	 *     @type bool   'do_support_classic' Whether the block editor can be switched to the classic editor.
	 * }
	 * @return bool True if the meta key was successfully registered, false if not.
	 */
	function add_field_block( array $args ): bool {
		return \wpinc\blok\field\add_block( $args );
	}

	/** phpcs:ignore
	 * Adds input block.
	 *
	 * phpcs:ignore
	 * @param array{
	 *     key                : string,
	 *     label              : string,
	 *     post_type          : string,
	 *     do_support_classic?: bool,
	 * } $args An array of arguments.
	 *
	 * $args {
	 *     An array of arguments.
	 *
	 *     @type string 'key'                Key of post meta.
	 *     @type string 'label'              Label of the post meta.
	 *     @type string 'post_type'          Target post type.
	 *     @type bool   'do_support_classic' Whether the block editor can be switched to the classic editor.
	 * }
	 * @return bool True if the meta key was successfully registered, false if not.
	 */
	function add_input_block( array $args ): bool {
		return \wpinc\blok\input\add_block( $args );
	}


	// -------------------------------------------------------------------------


	/**
	 * Adds 'small' tag button to the toolbar of heading blocks.
	 *
	 * @param string|null $url_to (Optional) URL to this script.
	 */
	function add_small_button_to_heading( ?string $url_to = null ): void {
		\wpinc\blok\add_small_button_to_heading( $url_to );
	}

	/**
	 * Adds list styles to the side panel of list blocks.
	 *
	 * @param string|null $url_to (Optional) URL to this script.
	 */
	function add_list_styles( ?string $url_to = null ): void {
		\wpinc\blok\add_list_styles( $url_to );
	}

	/**
	 * Sets used heading tags.
	 *
	 * @param int         $first_level First level of heading tag.
	 * @param int         $count       Count of headings.
	 * @param string|null $url_to      (Optional) URL to this script.
	 */
	function set_used_heading( int $first_level = 2, int $count = 3, ?string $url_to = null ): void {
		\wpinc\blok\set_used_heading( $first_level, $count, $url_to );
	}


	// -------------------------------------------------------------------------


	/**
	 * Unregisters block types.
	 *
	 * @param string|string[] $type_s A block type or an array of block types.
	 * @param string|null     $url_to (Optional) URL to this script.
	 */
	function unregister_block_type( $type_s, ?string $url_to = null ): void {
		\wpinc\blok\unregister_block_type( $type_s, $url_to );
	}

	/**
	 * Unregisters block categories.
	 *
	 * @param string|string[] $category_s A category or an array of categories.
	 * @param string|null     $url_to     (Optional) URL to this script.
	 */
	function unregister_block_category( $category_s, ?string $url_to = null ): void {
		\wpinc\blok\unregister_block_category( $category_s, $url_to );
	}

	/**
	 * Unregisters block variations.
	 *
	 * @param string          $type        A block type.
	 * @param string|string[] $variation_s A variation or an array of variations.
	 * @param string|null     $url_to      (Optional) URL to this script.
	 */
	function unregister_block_variation( string $type, $variation_s, ?string $url_to = null ): void {
		\wpinc\blok\unregister_block_variation( $type, $variation_s, $url_to );
	}

	/**
	 * Unregisters block styles.
	 *
	 * @param string          $type    A block type.
	 * @param string|string[] $style_s A style or an array of styles.
	 * @param string|null     $url_to  (Optional) URL to this script.
	 */
	function unregister_block_style( string $type, $style_s, ?string $url_to = null ): void {
		\wpinc\blok\unregister_block_style( $type, $style_s, $url_to );
	}
}
